//
//  Card.swift
//  Concentration
//
//  Created by Vlad on 2/15/20.
//  Copyright © 2020 Vlad. All rights reserved.
//

import Foundation

struct Card: Hashable {
    var hash: Int {return identifier}
    static func ==(lhs: Card, rhs: Card) -> Bool {
        return lhs.identifier == rhs.identifier
    }
    
    var isFaceUp = false
    var isMatched = false
    private var identifier: Int
    
    private static var identifierFactory = 0
    private static func getUniqIdenti() -> Int {
        identifierFactory += 1
        return identifierFactory
    }
    
    init() {
        self.identifier = Card.getUniqIdenti()
    }
}
